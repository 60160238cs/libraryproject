package libraryproject;

import database.Database;
import database.User;
import database.UserDAO;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.concurrent.locks.Condition;
import java.util.logging.Level;
import java.util.logging.Logger;


public class LibraryProject {
    static Scanner kb = new Scanner(System.in);
    public static void main(String[] args) {
        while(true){
            System.out.print("\t Menu Database"
                    +"\n1.user"
                    +"\n2.book (unavailable)"
                    +"\nSelect menu: ");
            int selectMenu = kb.nextInt();
            if(selectMenu==1){
                selectMenuUser();
            }
            if(selectMenu==2){
                //selectMenuBook();
                continue;
            }
        }
            
    }

    private static void selectMenuUser() {
        System.out.println("");
        showMenu();
        int selectMenu = kb.nextInt();
        if(selectMenu==1){
            insertUser();
        }else if(selectMenu==4){
            showUsers();
        }else if(selectMenu==0){
        }
    }
    
    private static void selectMenuBook() {
        showMenu();
        int selectMenu = kb.nextInt();
        if(selectMenu==1){
            //insertBook();
        }else if(selectMenu==4){
            //showBooks();
        }else if(selectMenu==0){
        }
    }

    private static void showUsers() {
        System.out.println("\n\tShow Users");
        ArrayList<User> list = UserDAO.getUsers();
        for(User user:list){
            System.out.println(user);
        }
        System.out.print("Any input for back to menu: ");
                kb.next();
                System.out.println();
    }

    private static void insertUser() {
        User newUser = new User();
        newUser.setUserId(-1);
        newUser.setLoginName("ABC");
        newUser.setPassword("password");
        newUser.setName("ABC");
        newUser.setSurname("DEF");
        newUser.setTypeId(1);
        UserDAO.insert(newUser);
        System.out.println();
    }

    private static void showMenu() {
        System.out.print("\tMenu User"
                + "\n1.Insert"
                + "\n2.Update (unavailable)"
                + "\n3.Delete (unavailable)"
                + "\n4.Show"
                + "\n0.Exit"
                + "\nSelect menu: ");
    }
}
