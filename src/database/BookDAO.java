package database;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import libraryproject.LibraryProject;

public class BookDAO {
    public static boolean insertBook(Book book){
        Connection conn = Database.connect();
        try {
            Statement stm = conn.createStatement();
            String sql="INSERT INTO book (\n" +
"                     name,\n" +
"                     bookId\n" +
"                 )\n" +
"                 VALUES (\n" +
"                     '%s',\n" +
"                     %d\n" +
"                 );";
            stm.execute(String.format(sql, book.getName(), book.getBookId()));
            Database.close();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        Database.close();
        return true;
    }
    public static boolean update(User user){
        return true;
    }
    public static boolean delete(User user){
        return true;
    }
    public static ArrayList<Book> getBooks(){
        ArrayList<Book> list = new ArrayList();
        Connection conn = Database.connect();
        try {
            Statement stm;
            stm = conn.createStatement();
            String sql = "SELECT bookId,\n" +
"                       name\n" +
"                       FROM book";
        
            ResultSet rs = stm.executeQuery(sql);
            while(rs.next()){
                System.out.println(rs.getInt("bookId")+ " " + rs.getString("bookName"));
                Book book = toObject(rs);
                list.add(book);
                
            }
            Database.close();
            return list;
        } catch (SQLException ex) {
            Logger.getLogger(LibraryProject.class.getName()).log(Level.SEVERE, null, ex);
        }
        Database.close();
        return null;
    }

    private static Book toObject(ResultSet rs) throws SQLException {
        Book book;
        book = new Book();
        book.setBookId(rs.getInt("bookId"));
        book.setName(rs.getString("bookName"));
        return book;
    }
    public User getBooks(int bookId){
        return null;
    }
}
